#include <Arduino.h>
#include "Debounce.h"

/**
 * Check https://www.arduino.cc/en/Tutorial/BuiltInExamples/Debounce
 */

Debounce::Debounce(
        int pin,
        unsigned long delay,
        bool isBlocking
) {
    this->pin = pin;
    this->delayMillis = delay;
    this->isBlocking = isBlocking;
}

void Debounce::setup(uint8_t mode) {
    pinMode(pin, mode);
}

void Debounce::setOnLowCallback(ExternalCallbackPointer onLowCallback) {
    this->onLowCallback = onLowCallback;
}

void Debounce::setOnHighCallback(ExternalCallbackPointer onHighCallback) {
    this->onHighCallback = onHighCallback;
}

void Debounce::loop() {
    if (this->isBlocking) {
        this->loopBlocking();
    } else {
        this->loopNonBlocking();
    }
}

void Debounce::loopNonBlocking() {
    if (this->isDebouncing) {
        unsigned long currentMillis = millis();
        if ((currentMillis - this->lastMillis) >= this->delayMillis) {
            this->isDebouncing = false;
        }
    } else {
        bool newState = digitalRead(this->pin);
        bool stateChanged = newState != this->state;

        if (stateChanged) {
            this->state = newState;
            this->isDebouncing = true;
            this->lastMillis = millis();
            if (this->state == HIGH) {
                if (this->onHighCallback != nullptr) {
                    this->onHighCallback();
                }
            } else {
                if (this->onLowCallback != nullptr) {
                    this->onLowCallback();
                }
            }
        }
    }
}

void Debounce::loopBlocking() {
    bool newState = digitalRead(this->pin);
    bool stateChanged = newState != this->state;

    if (stateChanged) {
        this->state = newState;
        delay(this->delayMillis);

        if (this->state == HIGH) {
            if (this->onHighCallback != nullptr) {
                this->onHighCallback();
            }
        } else {
            if (this->onLowCallback != nullptr) {
                this->onLowCallback();
            }
        }
    }
}

bool Debounce::getState() {
    return this->state;
}

bool Debounce::isLow() {
    return this->state == LOW;
}

bool Debounce::isHigh() {
    return this->state == HIGH;
}