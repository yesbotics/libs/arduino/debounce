#ifndef HIGH_POWER_SWITCH_ARDUINO_Debounce_H
#define HIGH_POWER_SWITCH_ARDUINO_Debounce_H

class Debounce {
public:
    typedef void (*ExternalCallbackPointer)();

    Debounce(
            int pin,
            unsigned long delay = 0,
            bool isBlocking = false
    );

    void setup(uint8_t mode = INPUT);

    void loop();

    void setOnLowCallback(ExternalCallbackPointer onLowCallback);

    void setOnHighCallback(ExternalCallbackPointer onHighCallback);

    bool getState();

    bool isLow();

    bool isHigh();

protected:

private:

    ExternalCallbackPointer onHighCallback = nullptr;
    ExternalCallbackPointer onLowCallback = nullptr;
    unsigned long delayMillis = 0;
    bool isDebouncing = false;
    unsigned long lastMillis;
    bool state = LOW;
    bool isBlocking = false;
    int pin;

    void loopNonBlocking();

    void loopBlocking();
};

#endif //HIGH_POWER_SWITCH_ARDUINO_Debounce_H
